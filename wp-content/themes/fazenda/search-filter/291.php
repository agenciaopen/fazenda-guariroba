<?php

/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if (!defined('ABSPATH')) {
	exit;
}

if ($query->have_posts()) {
?>



	<div class="pagination">

		<div class="nav-previous"><?php next_posts_link('Older posts', $query->max_num_pages); ?></div>
		<div class="nav-next"><?php previous_posts_link('Newer posts'); ?></div>
		<?php
		/* example code for using the wp_pagenavi plugin */
		if (function_exists('wp_pagenavi')) {
			echo "<br />";
			wp_pagenavi(array('query' => $query));
		}
		?>
	</div>
	<div class="row">
		<?php
		while ($query->have_posts()) {
			$query->the_post();

		?>
			<div class="container my-2">
				<div class="media row">
					<svg width="36" height="52" viewBox="0 0 36 52" fill="none" xmlns="http://www.w3.org/2000/svg" class="col-2 col-md-1 busca-img">
						<path d="M1.84424 17.6779C1.87801 30.4733 14.0698 32.5665 17.6835 50.1559C21.2971 32.5327 33.489 30.4733 33.5227 17.6779C33.5227 17.6779 33.5227 17.6779 33.5227 17.6441C33.5227 17.6441 33.5227 17.6441 33.5227 17.6104C33.489 8.90004 26.4305 1.84399 17.6835 1.84399C8.93644 1.84399 1.87801 8.90004 1.84424 17.6104C1.84424 17.6441 1.84424 17.6441 1.84424 17.6779Z" stroke="black" stroke-width="3" stroke-miterlimit="10" stroke-linejoin="round" />
						<path d="M17.683 23.1471C20.7978 23.1471 23.3229 20.6229 23.3229 17.509C23.3229 14.3952 20.7978 11.871 17.683 11.871C14.5681 11.871 12.043 14.3952 12.043 17.509C12.043 20.6229 14.5681 23.1471 17.683 23.1471Z" stroke="black" stroke-width="1.48" stroke-miterlimit="10" stroke-linejoin="round" />
					</svg>
					<div class="media-body col-8 p-0 col-md-10 mt-2">
						<h2 class="mt-0 busca-h2"><?php the_title(); ?></h2>
						<p class="mt-0 busca-p"><?php the_field('endereco', false, false); ?></p>
						<br><br>
						<p class="mt-0 busca-p"><?php the_field('telefone', false, false); ?></p>
					</div>
				</div>
			</div>
			<hr />
		<?php
		}
		?>
	</div>

	<div class="pagination">

		<div class="nav-previous"><?php next_posts_link('Older posts', $query->max_num_pages); ?></div>
		<div class="nav-next"><?php previous_posts_link('Newer posts'); ?></div>
		<?php
		/* example code for using the wp_pagenavi plugin */
		if (function_exists('wp_pagenavi')) {
			echo "<br />";
			wp_pagenavi(array('query' => $query));
		}
		?>
	</div>
<?php
} else {
	echo "No Results Found";
}
?>