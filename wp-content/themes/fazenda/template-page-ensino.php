<?php

/**
 *
 * Template Name: Ensino
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php if (wp_is_mobile()) : ?>
<?php if (get_field('secao_1_background', $post->ID)) :
        $bg = get_field('secao_1_background', $post->ID);
    endif;
else :
    $bg = get_field('secao_1_background_desktop', $post->ID);
endif; ?>

<section class="ensino-cultive" style="background-image: url('<?php echo $bg; ?>')">
    <div class="container">
        <article class="ensino-cultive-article">
            <h1 class="ensino-cultive-titulo mb-4">
                <?php the_field('secao_1_titulo'); ?>
            </h1>
            <p class="ensino-cultive-texto">
                <?php the_field('secao_1_subtitulo', false, false); ?>
            </p>
        </article>
    </div>
</section>

<section class="ensino-conheca">
    <article class="ensino-conheca-article">
        <h2 class="ensino-conheca-titulo">
            <?php the_field('secao_2_titulo'); ?>
        </h2>
        <p class="ensino-conheca-texto">
            <?php the_field('secao_2_texto', false, false); ?>
        </p>
        <?php if (get_field('secao_2_botao_para_agendar_visita') == 1) : ?>
            <button class="ensino-conheca-botao" type="submit" id="agendaVisita"><span class="botao-seta">&#10095;</span> Agendar uma visita</button>
            <?php // echo 'true'; 
            ?>
        <?php else : ?>
            <?php // echo 'false'; 
            ?>
        <?php endif; ?>

        <div class="modal" id="modal">
            <div class="modal-dialog">
                <section class="modal-conteudo">
                    <span class="modal-header">
                        <h2 class="modal-titulo">Agende sua visita</h2>
                        <button class="modal-botao" aria-label="fechar a janela" id="fechaModal">&#10006;</button>
                    </span>
                    <div class="modal-form">
                        <?php echo do_shortcode('[contact-form-7 id="254" title="Modal formulário"]'); ?>
                    </div>

                </section>
                <img src="<?php echo get_template_directory_uri(); ?>/imgs/modal.png" alt="" srcset="" class="modal-img">
            </div>
        </div>
    </article>

    <div class="ensino-conheca-imgs-desktop">
        <?php if (get_field('secao_2_imagem_1')) : ?>
            <img src="<?php the_field('secao_2_imagem_1'); ?>" class="ensino-conheca-img-cima-desktop" />
        <?php endif ?>
        <?php if (get_field('secao_2_imagem_2')) : ?>
            <img src="<?php the_field('secao_2_imagem_2'); ?>" class="ensino-conheca-img-baixo" />
        <?php endif ?>
    </div>
</section>


<section class="ensino-atencao">
    <div class="container">
        <article class="ensino-atencao-article">
            <h2 class="ensino-atencao-titulo">
                <?php the_field('aviso_titulo'); ?>
            </h2>
            <p class="ensino-atencao-texto">
                <?php the_field('aviso_texto', false, false); ?>
                <a href="#inscreva" rel="noopener noreferrer" class="ensino-atencao-link">
                    Basta clicar aqui!
                </a>
            </p>
        </article>
    </div>
</section>
<section class="embed-responsive embed-responsive-16by9">
    <?php the_field('secao_4_video'); ?>
</section>
<section class="ensino-colheita">
    <div class="container">
        <article class="ensino-colheita-article">
            <h2 class="ensino-colheita-titulo">
                <?php the_field('secao_4_titulo'); ?>
            </h2>
            <p class="ensino-colheita-texto">
                <?php the_field('secao_4_texto'); ?>
            </p>
            <figure class="ensino-colheita-imgs">

                <div style="background-image: url('<?php the_field('secao_4_imagem_1'); ?>')" class="ensino-colheita-img">
                    <?php $secao_4_botao_1 = get_field('secao_4_botao_1'); ?>
                    <?php if ($secao_4_botao_1) : ?>
                        <a class="ensino-colheita-botao" href="<?php echo esc_url($secao_4_botao_1['url']); ?>">Roda de sabores do provador de café</a>
                    <?php endif; ?>
                </div>
                <div style="background-image: url('<?php the_field('secao_4_imagem_2'); ?>')" class="ensino-colheita-img">
                    <?php $secao_4_botao_2 = get_field('secao_4_botao_2'); ?>
                    <?php if ($secao_4_botao_2) : ?>
                        <a class="ensino-colheita-botao" href="<?php echo esc_url($secao_4_botao_2['url']); ?>">Café Guariroba no Japão</a>
                    <?php endif; ?>
                </div>

            </figure>
        </article>
    </div>
</section>


<?php get_footer(); ?>