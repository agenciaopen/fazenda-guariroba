<?php

/**
 *
 * Template Name: Cafés
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php if (wp_is_mobile()) : ?>
<?php if (get_field('secao_1_cafes_especiais_background_mobile', $post->ID)) :
        $bg = get_field('secao_1_cafes_especiais_background_mobile', $post->ID);
    endif;
else :
    $bg = get_field('secao_1_cafes_especiais_background_desktop', $post->ID);
endif; ?>

<section class="nossos-cafes" style="background-image: url('<?php echo $bg; ?>')">
    <div class="container d-lg-flex flex-lg-column space-between-around">
        <article class="nossos-cafes-article col-lg-8">
            <h1 class="nossos-cafes-titulo col-lg-10 p-0 mb-4"><?php the_field('secao_1_cafes_especiais_titulo'); ?></h1>
        </article>
        <div class="p-0 align-self-end col-lg-10">
            <?php get_template_part('templates/global/template-part', 'selos'); ?>
        </div>
    </div>

</section>

<section class="nossos-cafes-detalhes">
    <div class="container">
        <article class="nossos-cafes-detalhes-article">
            <h2 class="nossos-cafes-detalhes-titulo"><?php the_field('secao_2_cafes_especiais_titulo'); ?></h2>

            <div class="nossos-cafes-detalhes-figure">
                <?php if (have_rows('secao_2_cafes_especiais_selos_de_qualidade')) : ?>
                    <?php while (have_rows('secao_2_cafes_especiais_selos_de_qualidade')) : the_row(); ?>
                        <figure class="nossos-cafes-detalhes-imgs">
                            <?php if (get_sub_field('selo')) : ?>
                                <img src="<?php the_sub_field('selo'); ?>" alt="" srcset="" class="nossos-cafes-detalhes-img " lazy="loading">
                            <?php endif ?>
                            <p class="nossos-cafes-detalhes-texto mt-2"> <?php the_sub_field('selo_texto'); ?></p>
                        </figure>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found 
                    ?>
                <?php endif; ?>
            </div>

        </article>
        <div class="d-flex flex-column flex-md-row align-items-center align-content-center justify-content-around">
            <?php $secao_2_cafes_especiais_mais_detalhes = get_field('secao_2_cafes_especiais_mais_detalhes'); ?>
            <?php if ($secao_2_cafes_especiais_mais_detalhes) : ?>
                <a class="botao-detalhes col-md-3" href="<?php echo esc_url($secao_2_cafes_especiais_mais_detalhes['url']); ?>"><?php echo esc_html($secao_2_cafes_especiais_mais_detalhes['title']); ?></a>
            <?php endif; ?>
            <?php
            $phone = get_field('whatsapp', 'option');
            $phone = preg_replace('/\D+/', '', $phone);
            $message = rawurldecode(get_field('botao_experiencia_para_os_clientes', 'option'));
            ?>
            <a href="https://wa.me/55<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank" class="align-self-center">
                <button class="mundo-botao" type="submit">
                    <?php the_field('botao_experiencia_para_os_clientes', 'option'); ?>
                </button>
            </a>
        </div>
    </div>
</section>

<?php get_template_part('templates/global/template-part', 'produto-slide'); ?>
<section class="nossos-cafes-especiais" style="background-color: #000;background-image: url('<?php the_field('secao_3_cafes_especiais_imagem_1'); ?>');">
    <div class="container-fluid">
        <div class="row justify-content-end">
            <div class="col-xl-6 col-lg-12 nossos-cafes-especiais-article pl-4" style="background-color: #000;">
                <h2 class="nossos-cafes-especiais-titulo">
                    <?php the_field('secao_3_cafes_especiais_titulo'); ?>
                </h2>
                <p class="nossos-cafes-especiais-texto">
                    <?php the_field('secao_3_cafes_especiais_texto', false, false); ?>
                </p>
            </div>
            <div class="col-xl-6 col-lg-12 p-0 ">
                <img src="<?php the_field('secao_3_cafes_especiais_imagem_1'); ?>" alt="" class="img-fluid w-100 d-xl-none">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <h2 class="nossos-cafes-especiais-titulo-dois">
            <?php the_field('secao_3_cafes_especiais_subtitulo'); ?>
        </h2>

        <p class="nossos-cafes-especiais-texto-dois">
            <?php the_field('secao_3_cafes_especiais_texto_secundario', false, false); ?>
        </p>
        <h3 class="nossos-cafes-especiais-titulo-dois">
            <?php the_field('secao_3_cafes_especiais_subtitulo_2'); ?>
        </h3>
        <div class="d-block d-lg-flex flex-wrap">
            <?php if (have_rows('secao_3_cafes_especiais_itens')) : ?>
                <?php while (have_rows('secao_3_cafes_especiais_itens')) : the_row(); ?>
                    <figure class="nossos-cafes-especiais-metodos col-12 p-0 col-lg-6 my-4">
                        <?php if (get_sub_field('secao_3_cafes_especiais_itens_imagem')) : ?>
                            <img src="<?php the_sub_field('secao_3_cafes_especiais_itens_imagem'); ?>" class="p-0 col-2 col-md-1 mx-auto my-2 my-md-auto mx-md-2" />
                        <?php endif ?>
                        <div>
                            <p class="nossos-cafes-especiais-metodos-titulo">
                                <?php the_sub_field('secao_3_cafes_especiais_itens_titulo'); ?>
                            </p>
                            <p class="nossos-cafes-especiais-metodos-texto">
                                <?php the_sub_field('secao_3_cafes_especiais_itens_texto'); ?>
                            </p>
                        </div>
                    </figure>
                <?php endwhile; ?>

            <?php else : ?>
                <?php // no rows found 
                ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_template_part('templates/global/template-part', 'mundo'); ?>

<?php get_template_part('templates/global/template-part', 'qualidade-selo'); ?>
<?php get_footer(); ?>