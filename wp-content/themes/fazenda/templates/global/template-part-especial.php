<section class="home-infos">
    <div class="container">
        <article class="">
            <h2 class="home-infos-titulo">
                <?php the_field('titulo_por_que_nosso_cafe_e_especial', 'option'); ?>
            </h2>
            <p class="home-infos-texto m-0">
                <?php the_field('descricao_por_que_nosso_cafe_e_especial', 'option', false, false); ?>
            </p>
            <?php $botao_por_que_nosso_cafe_e_especial = get_field('botao_por_que_nosso_cafe_e_especial', 'option'); ?>
            <?php if ($botao_por_que_nosso_cafe_e_especial) : ?>
                <a class="home-infos-botao" href="<?php echo esc_url($botao_por_que_nosso_cafe_e_especial['url']); ?>" target="<?php echo esc_attr($botao_por_que_nosso_cafe_e_especial['target']); ?>">
                    <span class="botao-seta">&#10095; </span>
                        <?php echo esc_html($botao_por_que_nosso_cafe_e_especial['title']); ?>
                </a>
            <?php endif; ?>
        </article>
    </div>
</section>