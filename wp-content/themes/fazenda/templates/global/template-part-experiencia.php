<section class="home-info-outra" style="background-color: #000;background-image: url('<?php the_field( 'imagem_experiencia_para_os_clientes', 'option' ); ?>');">
    <div class="container">
        <article class="home-info-outra-article">
        
                <h2 class="home-info-outra-titulo"><?php the_field( 'titulo_experiencia_para_os_clientes', 'option' ); ?></h2>
                <p class="home-info-outra-texto"><?php the_field( 'descricao_experiencia_para_os_clientes', 'option', false, false  ); ?>
                </p>
                <?php 
                    $phone = get_field( 'whatsapp', 'option' );
                    $phone = preg_replace('/\D+/', '', $phone);
                    $message = rawurldecode(get_field( 'botao_experiencia_para_os_clientes', 'option' ));
                ?>
                <a href="https://wa.me/55<?php echo $phone; ?>?text=<?php echo $message;?>" rel="external" target="_blank">
                    <button class="home-info-outra-botao" type="submit"> 
                        <span class="botao-seta">&#10095;</span>
                        <?php the_field( 'botao_experiencia_para_os_clientes', 'option' ); ?> 
                    </button>
                </a>
                

            </article>
    </div>
        
 
</section>