<section class="mundo main">
    <div class="container d-flex flex-column">
        <p class="mundo-titulo">Café Guariroba pelo mundo</p>
        <div class="mundo-carrousel mb-4">
            <?php
            $args = array("posts_per_page" => -1, "post_type" => "lugares");
            $posts_array = get_posts($args);
            foreach ($posts_array as $post) { ?>
                <div class="mx-4">
                    <div>
                        <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>
                        <img src=" <?php echo $url ?>" alt="" srcset="" class="img-fluid mb-2">

                        <p class="mundo-texto"><?php echo $post->post_title; ?> </p>
                    </div>
                </div>
            <?php } ?>
            <?php wp_reset_postdata(); ?>
        </div>
        <?php
        $phone = get_field('whatsapp', 'option');
        $phone = preg_replace('/\D+/', '', $phone);
        $message = rawurldecode(get_field('botao_experiencia_para_os_clientes', 'option'));
        ?>
        <a href="https://wa.me/55<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank" class="align-self-center">
            <button class="mundo-botao" type="submit">
                <?php the_field('botao_experiencia_para_os_clientes', 'option'); ?>
            </button>
        </a>
    </div>
</section>