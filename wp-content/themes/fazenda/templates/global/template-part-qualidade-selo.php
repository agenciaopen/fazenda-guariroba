<section class="qualidade-selo">
    <div class="container">
        <article class="qualidade-selo-article">
            <h2 class="qualidade-selo-titulo"><?php the_field('titulo_cultivamos_qualidade', 'option'); ?></h2>
            <div class="carousel-selo">
                <?php if (have_rows('cadastro_de_itens_-_cultivamos_qualidade', 'option')) : ?>
                    <?php $count = 0;
                    while (have_rows('cadastro_de_itens_-_cultivamos_qualidade', 'option')) : the_row(); ?>

                        <?php
                        if ($count == 1) :
                            $class = 'qualidade-selo-img-sp';
                        elseif (($count == 2)) :
                            $class = 'qualidade-selo-img';
                        elseif (($count == 3) || ($count == 4)) :
                            $class = 'qualidade-selo-img';
                        else :
                            $class = 'qualidade-selo-img-nw';
                        endif;
                        ?>
                        <figure class="qualidade-selo-card">
                            <img src="<?php the_sub_field('imagem_cultivamos_qualidade'); ?>" alt="" srcset="" class="<?php echo $class; ?>" />
                            <p class="qualidade-selo-texto"><?php the_sub_field('descricao_cultivamos_qualidade', false, false); ?></p>
                        </figure>
                    <?php $count++;
                    endwhile; ?>
                <?php else : ?>
                    <?php // no rows found 
                    ?>
                <?php endif; ?>
            </div>
        </article>
    </div>
</section>