
<section class="produto-slide">
    <div class="slide-color">
        <div class="container">
            <article class="produto-slide-article">
                <h2 class="produto-slide-titulo"><?php the_field( 'titulo_produtos', 'option' ); ?></h2>
                <div class="produto-carousel">
                    <?php
                    $args = array("posts_per_page" => -1, "post_type" => "cafes");
                    $posts_array = get_posts($args);
                    foreach($posts_array as $post) { ?>
                        <div class="produto-slide-cards">
                            <figure class="produto-slide-card">
                                <img src="<?php echo the_field('imagem_da_embalagem');?>" alt="" srcset="" class="produto-slide-card-img">
                                <p class="produto-slide-card-texto"><?php echo $post->post_title;?> <br><br>
                                <?php echo the_field('peso');?> gramas</p>
                            </figure>
                            <?php
                            $phone = get_field('whatsapp', 'option');
                            $phone = preg_replace('/\D+/', '', $phone);
                            ?>
                            <a href="https://api.whatsapp.com/send?phone=55<?php echo $phone; ?>" rel="noopener noreferrer" target="_blank">
                                    <button class="produto-slide-botao">
                                    Comprar
                                    </button>
                            </a>  
                        </div>
                        <?php wp_reset_postdata(); ?>           
                    <?php } ?>                    
                </div>    
            </article>
        </div>
    </div>
</section>


