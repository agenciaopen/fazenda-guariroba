<section class="busca">
    <div class="container align-items-center">
        <h2 class="busca-titulo"><?php the_field('titulo_nosso_sabor_mais_perto_de_voce', 'option'); ?></h2>
        <p class="busca-texto"><?php the_field('descricao_nosso_sabor_mais_perto_de_voce', 'option'); ?></p>
        <div class="busca-form col-12">
            <?php echo do_shortcode('[searchandfilter id="291"]'); ?>
        </div>
        <div class="busca-form" >
            <?php echo do_shortcode('[searchandfilter id="291" show="results"]'); ?>
        </div>

        <?php if (get_field('botao_nosso_sabor_mais_perto_de_voce', 'option')) : ?>
            <a href="<?php the_field('botao_nosso_sabor_mais_perto_de_voce', 'option'); ?>" target="_blank">

                <div class="busca-botao m-auto">
                    Lista de parceiros
                </div>
            </a>
        <?php endif; ?>
    </div>
</section>