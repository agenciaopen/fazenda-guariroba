<figure class="selos">
    <?php if (have_rows('cadastro_de_itens_-_cultivamos_qualidade', 'option')) : ?>
        <?php $count = 1;
        while (have_rows('cadastro_de_itens_-_cultivamos_qualidade', 'option')) : the_row(); ?>
            <?php if (get_sub_field('imagem_cultivamos_qualidade')) : ?>
                <?php
                if ($count == 2) :
                    $class = 'selo-um';
                elseif (($count == 4) || ($count == 5)) :
                    $class = 'selo-tres';
                elseif (($count == 3)) :
                    $class = 'selo-dois';
                else :
                    $class = 'selo-quatro';
                endif;
                ?>
                <img class="<?php echo $class; ?>" src="<?php the_sub_field('imagem_cultivamos_qualidade'); ?>" class="col-8" />
            <?php endif; ?>
        <?php $count++;
        endwhile; ?>
    <?php else : ?>
        <?php // no rows found 
        ?>
    <?php endif; ?>
</figure>