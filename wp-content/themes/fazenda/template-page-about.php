<?php

/**
 *
 * Template Name: Nossa Fazenda
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php if (wp_is_mobile()) : ?>
<?php if (get_field('secao_1_-_nossa_fazenda_background_mobile', $post->ID)) :
        $bg = get_field('secao_1_-_nossa_fazenda_background_mobile', $post->ID);
    endif;
else :
    $bg = get_field('secao_1_-_nossa_fazenda_background', $post->ID);
endif; ?>

<section class="about" style="background-image: url('<?php echo $bg; ?>')">
    <div class="container">
        <article class="about-article">
            <h1 class="about-titulo mb-4">
                <?php the_field('secao_1-_nossa_fazenda_titulo'); ?>
            </h1>
            <p class="about-texto col-12 p-0">
                <?php the_field('secao_1_-_nossa_fazenda_texto', false, false); ?>
            </p>
            <?php $secao_1_nossa_fazenda_botao = get_field('secao_1_-_nossa_fazenda_botao'); ?>
            <?php if ($secao_1_nossa_fazenda_botao) : ?>
                <a class="about-botao" href="<?php echo esc_url($secao_1_nossa_fazenda_botao); ?>">Conhecer produtos</a>
            <?php endif; ?>
        </article>
    </div>
</section>

<section class="about-localidade">
    <div class="container">
        <span class="about-localidade-desktop">
            <article class="about-localidade-article">
                <h2 class="about-localidade-titulo">
                    <?php the_field('secao_2_nossa_fazenda_titulo'); ?>
                </h2>
                <p class="about-localidade-texto">
                    <?php the_field('secao_2_nossa_fazenda_texto', false, false); ?>
                </p>

                <!-- texto desktop -->
                <div class="texto-desktop">
                    <h2 class="about-localidade-titulo-dois">
                        <?php the_field('secao_2_nossa_fazenda_subtitulo'); ?>
                    </h2>
                    <?php if (have_rows('secao_2_nossa_fazenda_itens')) : ?>
                        <?php while (have_rows('secao_2_nossa_fazenda_itens')) : the_row(); ?>
                            <p class="about-localidade-texto-dois">
                                <span class="font-weight-bold">
                                    <?php the_sub_field('item_titulo'); ?>
                                </span>
                                <br>
                            <p class="about-localidade-texto-dois">
                                <?php the_sub_field('item_texto'); ?>
                            </p>
                            <br>
                            <br>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
                <!-- texto desktop -->
            </article>
            <div class="about-localidade-img-div-mapa">
                <?php if (get_field('secao_2_nossa_fazenda_mapa')) : ?>
                    <img src="<?php the_field('secao_2_nossa_fazenda_mapa'); ?>" class="about-localidade-img-mapa" />
                <?php endif ?>
            </div>

            <?php if (get_field('secao_2_nossa_fazenda_mapa')) : ?>
                <img src="<?php the_field('secao_2_nossa_fazenda_mapa'); ?>" class="about-localidade-img-mapa-desktop" />
            <?php endif ?>
        </span>
        <div class="texto-mobile">
            <h2 class="about-localidade-titulo-dois">
                <?php the_field('secao_2_nossa_fazenda_subtitulo'); ?>
            </h2>
            <?php if (have_rows('secao_2_nossa_fazenda_itens')) : ?>
                <?php while (have_rows('secao_2_nossa_fazenda_itens')) : the_row(); ?>
                    <p class="about-localidade-texto-dois">
                        <span class="font-weight-bold">
                            <?php the_sub_field('item_titulo'); ?>
                        </span>
                        <br>
                    <p class="about-localidade-texto-dois">
                        <?php the_sub_field('item_texto'); ?>
                    </p>
                    <br>
                    <br>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found 
                ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_template_part('templates/global/template-part', 'produto-slide'); ?>

<section class="about-descubra">
    <div class="container">
        <article class="about-descubra-article">
            <div class="about-descubra-paragrafo align-content-center">
                <h2 class="about-descubra-titulo">
                    <?php the_field('secao_3_nossa_fazenda_titulo', $page_ID); ?>
                </h2>
                <p class="about-descubra-texto">
                    <?php the_field('secao_3_nossa_fazenda_texto', $page_ID); ?>
                </p>
                <?php $secao_3_nossa_fazenda_botao = get_field('secao_3_nossa_fazenda_botao'); ?>
                <?php if ($secao_3_nossa_fazenda_botao) : ?>
                    <a class="about-descubra-botao" href="<?php echo esc_url($secao_3_nossa_fazenda_botao); ?>">Quero saber mais</a>
                <?php endif; ?>
            </div>


            <div class="about-descubra-img-div">
                <?php if (get_field('secao_3_nossa_fazenda_imagem', $page_ID)) : ?>
                    <img src="<?php the_field('secao_3_nossa_fazenda_imagem', $page_ID); ?>" class="about-descubra-img" lazy="loading" />
                <?php endif ?>
            </div>
        </article>
    </div>
</section>


<?php get_template_part('templates/global/template-part', 'mundo'); ?>
<?php get_template_part('templates/global/template-part', 'qualidade-selo'); ?>
<section class="main instagram">
    <div class="container">
        <h2 class="instagram__titulo">Siga-nos no Instagram</h2>

        <?php echo do_shortcode('[instagram-feed]'); ?>
    </div>
</section>
<?php get_footer(); ?>