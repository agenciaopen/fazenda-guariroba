<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Configurações gerais',
		'menu_title'	=> 'Configurações gerais',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}
// admin config acf

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
// featured image pages
add_filter( 'use_block_editor_for_post', '__return_false' );

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/inc/navbar/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

function prefix_modify_nav_menu_args( $args ) {
    return array_merge( $args, array(
        'walker' => new WP_Bootstrap_Navwalker(),
    ) );
}
add_filter( 'wp_nav_menu_args', 'prefix_modify_nav_menu_args' );

/**
 * Register nav menu primary
 */
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'h2' ),
) );

/**
 * enqueue global styles
 */
function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'slick-h2', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css', array(), rand(111,9999), 'all'  );
	wp_enqueue_style( 'slickt-h2', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css', array(), rand(111,9999), 'all'  );
	wp_enqueue_style( 'global-h2', get_template_directory_uri() . '/css/style.min.css', array(), rand(111,9999), 'all'  );
	wp_enqueue_style( 'font-principal', 'https://fonts.googleapis.com/css2?family=Epilogue:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap', array(), rand(111,9999), 'all'  );
	

	wp_deregister_script('jquery');
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true);
    wp_enqueue_script( 'bs4-h2', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),  '', 'all' );
	wp_enqueue_script('slick-h2-js', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js', array('jquery') , rand(111,9999), 'all' );
	
	
	
	wp_enqueue_script( 'global-h2-js', get_template_directory_uri() . '/js/global.js', array('jquery'),  rand(111,9999), 'all' );
	wp_enqueue_script( 'modal', get_template_directory_uri() . '/js/modal.js', array('jquery'),  rand(111,9999), 'all' );
	wp_enqueue_script( 'home-carrosel', get_template_directory_uri() . '/js/homeCarrosel.js', array('jquery'),  rand(111,9999), 'all' );
	
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

add_theme_support('post-thumbnails');


 /**
 * Remove wp tags
 */
 require_once('inc/remove-wp.php');

/**
* Custom login page
*/
require_once('inc/login-page.php');

/**
* Custom admin page
*/
require_once('inc/wp-admin.php');


/**
* Upload SVG files
*/
require_once('inc/svg-upload.php');

/**
* Remove emojis
*/
require_once('inc/remove-emojis.php');

/**
* Remove posts 
*/
require_once('inc/remove-posts.php');

/**
* Remove comments 
*/
require_once('inc/remove-comments.php');

add_action( 'add_meta_boxes', 'NAME_remove_meta_boxes', 100);
function NAME_remove_meta_boxes() {
	remove_meta_box( 'trackbacksdiv', 'post', 'normal' ); // Trackbacks meta box
	remove_meta_box( 'postcustom', 'post', 'normal' ); // Custom fields meta box
	remove_meta_box( 'commentsdiv', 'post', 'normal' ); // Comments meta box
	remove_meta_box( 'slugdiv', 'post', 'normal' );	// Slug meta box
	remove_meta_box( 'authordiv', 'post', 'normal' ); // Author meta box
	remove_meta_box( 'revisionsdiv', 'post', 'normal' ); // Revisions meta box
	remove_meta_box( 'formatdiv', 'post', 'normal' ); // Post format meta box
	remove_meta_box( 'commentstatusdiv', 'post', 'normal' ); // Comment status meta box
	remove_meta_box( 'categorydiv', 'post', 'side' ); // Category meta box
	remove_meta_box( 'tagsdiv-post_tag', 'post', 'side' ); // Post tags meta box
	remove_meta_box( 'pageparentdiv', 'post', 'side' ); // Page attributes meta box
	
	//remove_meta_box( 'file_gallery', 'post', 'normal'); // File Gallery Plugin
	//remove_meta_box( 'wpseo_meta', 'post', 'normal'); // YOAST Seo
	//remove_meta_box( 'gdsr-meta-box-mur', 'post', 'advanced');  // GD Star Rating
	//remove_meta_box( 'gdsr-meta-box', 'post', 'side' ); // GD Star Rating
	//remove_meta_box( 'yourlsdiv', 'post', 'side'); // YOURLS
}

// Replace 'post' with whatever post type you are trying to hide meta boxes for.


function cafes_custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Cafés', 'Post Type General Name', 'h2' ),
            'singular_name'       => _x( 'Café', 'Post Type Singular Name', 'h2' ),
            'menu_name'           => __( 'Cafés', 'h2' ),
            'parent_item_colon'   => __( 'Parent Café', 'h2' ),
            'all_items'           => __( 'Todos os Cafés', 'h2' ),
            'view_item'           => __( 'Ver Café', 'h2' ),
            'add_new_item'        => __( 'Adicionar novo Café', 'h2' ),
            'add_new'             => __( 'Adicionar novo', 'h2' ),
            'edit_item'           => __( 'Editar Café', 'h2' ),
            'update_item'         => __( 'Atualizar Café', 'h2' ),
            'search_items'        => __( 'Procurar Café', 'h2' ),
            'not_found'           => __( 'Nada encontrado', 'h2' ),
            'not_found_in_trash'  => __( 'Nada encontrada na lixeira', 'h2' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Cafés', 'h2' ),
            'description'         => __( 'Cadastro de Cafés e seus detalhes', 'h2' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-coffee',
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
      
        );
         
        // Registering your Custom Post Type
        register_post_type( 'cafes', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'cafes_custom_post_type', 0 );


	
function lugares_custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Lugares', 'Post Type General Name', 'h2' ),
            'singular_name'       => _x( 'Lugar', 'Post Type Singular Name', 'h2' ),
            'menu_name'           => __( 'Lugares', 'h2' ),
            'parent_item_colon'   => __( 'Parent Lugar', 'h2' ),
            'all_items'           => __( 'Todos os Lugares', 'h2' ),
            'view_item'           => __( 'Ver Lugar', 'h2' ),
            'add_new_item'        => __( 'Adicionar novo Lugar', 'h2' ),
            'add_new'             => __( 'Adicionar novo', 'h2' ),
            'edit_item'           => __( 'Editar Lugar', 'h2' ),
            'update_item'         => __( 'Atualizar Lugar', 'h2' ),
            'search_items'        => __( 'Procurar Lugar', 'h2' ),
            'not_found'           => __( 'Nada encontrado', 'h2' ),
            'not_found_in_trash'  => __( 'Nada encontrada na lixeira', 'h2' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Lugares', 'h2' ),
            'description'         => __( 'Cadastro de Lugares e seus detalhes', 'h2' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'author', 'thumbnail', 'revisions', 'custom-fields'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-admin-site',
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
      
        );
         
        // Registering your Custom Post Type
        register_post_type( 'lugares', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
	add_action( 'init', 'lugares_custom_post_type', 0 );


  function estabelecimentos_custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'estabelecimentos', 'Post Type General Name', 'h2' ),
            'singular_name'       => _x( 'Lugar', 'Post Type Singular Name', 'h2' ),
            'menu_name'           => __( 'estabelecimentos', 'h2' ),
            'parent_item_colon'   => __( 'Parent Lugar', 'h2' ),
            'all_items'           => __( 'Todos os estabelecimentos', 'h2' ),
            'view_item'           => __( 'Ver Lugar', 'h2' ),
            'add_new_item'        => __( 'Adicionar novo Lugar', 'h2' ),
            'add_new'             => __( 'Adicionar novo', 'h2' ),
            'edit_item'           => __( 'Editar Lugar', 'h2' ),
            'update_item'         => __( 'Atualizar Lugar', 'h2' ),
            'search_items'        => __( 'Procurar Lugar', 'h2' ),
            'not_found'           => __( 'Nada encontrado', 'h2' ),
            'not_found_in_trash'  => __( 'Nada encontrada na lixeira', 'h2' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'estabelecimentos', 'h2' ),
            'description'         => __( 'Cadastro de estabelecimentos e seus detalhes', 'h2' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'author', 'thumbnail', 'revisions', 'custom-fields'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-admin-site',
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
      
        );
         
        // Registering your Custom Post Type
        register_post_type( 'estabelecimentos', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
	add_action( 'init', 'estabelecimentos_custom_post_type', 0 );
	
add_action( 'init', 'create_emp_custom_taxonomy', 0 );

 //create a custom taxonomy name it "type" for your posts
 function create_emp_custom_taxonomy() {
     
	$labels = array(
	  'name' => _x( 'Estado', 'taxonomy general name' ),
	  'singular_name' => _x( 'Estado', 'taxonomy singular name' ),
	  'search_items' =>  __( 'Procurar por tipos' ),
	  'all_items' => __( 'Todos os tipos' ),
	  'parent_item' => __( 'Parent tipo' ),
	  'parent_item_colon' => __( 'Parent Type:' ),
	  'edit_item' => __( 'Editar tipo' ), 
	  'update_item' => __( 'Atualizar tipo' ),
	  'add_new_item' => __( 'Adicionar novo Estado' ),
	  'new_item_name' => __( 'Novo Estado' ),
	  'menu_name' => __( 'Estados' ),
	); 	
   
	register_taxonomy('estado',array('estabelecimentos'), array(
	  'hierarchical' => true,
	  'labels' => $labels,
	  'show_ui' => true,
	  'show_admin_column' => true,
	  'query_var' => true,
	  'rewrite' => array( 'slug' => 'estado' ),
	));
  }
  
  // Let us create Taxonomy for Custom Post Type
  add_action( 'init', 'create_emp_custom_taxonomy_service', 0 );
   
  //create a custom taxonomy name it "type" for your posts
  function create_emp_custom_taxonomy_service() {
   
	$labels = array(
	  'name' => _x( 'Cidade', 'taxonomy general name' ),
	  'singular_name' => _x( 'Cidade', 'taxonomy singular name' ),
	  'search_items' =>  __( 'Procurar por tipos' ),
	  'all_items' => __( 'Todos os tipos' ),
	  'parent_item' => __( 'Parent tipo' ),
	  'parent_item_colon' => __( 'Parent Type:' ),
	  'edit_item' => __( 'Editar tipo' ), 
	  'update_item' => __( 'Atualizar tipo' ),
	  'add_new_item' => __( 'Adicionar novo Cidade' ),
	  'new_item_name' => __( 'Novo Cidade' ),
	  'menu_name' => __( 'Cidades' ),
	); 	
   
	register_taxonomy('cidades',array('estabelecimentos'), array(
	  'hierarchical' => true,
	  'labels' => $labels,
	  'show_ui' => true,
	  'show_admin_column' => true,
	  'query_var' => true,
	  'rewrite' => array( 'slug' => 'cidades' ),
	));
  }

    // Let us create Taxonomy for Custom Post Type
	add_action( 'init', 'create_emp_custom_taxonomy_pais', 0 );
   
	//create a custom taxonomy name it "type" for your posts
	function create_emp_custom_taxonomy_pais() {
	 
	  $labels = array(
		'name' => _x( 'País', 'taxonomy general name' ),
		'singular_name' => _x( 'País', 'taxonomy singular name' ),
		'search_items' =>  __( 'Procurar por tipos' ),
		'all_items' => __( 'Todos os tipos' ),
		'parent_item' => __( 'Parent tipo' ),
		'parent_item_colon' => __( 'Parent Type:' ),
		'edit_item' => __( 'Editar tipo' ), 
		'update_item' => __( 'Atualizar tipo' ),
		'add_new_item' => __( 'Adicionar novo País' ),
		'new_item_name' => __( 'Novo País' ),
		'menu_name' => __( 'Países' ),
	  ); 	
	 
	  register_taxonomy('pais',array('estabelecimentos'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'pais' ),
	  ));
	}

remove_filter( 'the_content', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );

remove_filter ('acf_the_content', 'wpautop');

add_filter('wpcf7_autop_or_not', '__return_false');

//   class FLHM_HTML_Compression
// {
// protected $flhm_compress_css = true;
// protected $flhm_compress_js = true;
// protected $flhm_info_comment = true;
// protected $flhm_remove_comments = true;
// protected $html;
// public function __construct($html)
// {
// if (!empty($html))
// {
// $this->flhm_parseHTML($html);
// }
// }
// public function __toString()
// {
// return $this->html;
// }
// protected function flhm_bottomComment($raw, $compressed)
// {
// $raw = strlen($raw);
// $compressed = strlen($compressed);
// $savings = ($raw-$compressed) / $raw * 100;
// $savings = round($savings, 2);
// return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
// }
// protected function flhm_minifyHTML($html)
// {
// $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
// preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
// $overriding = false;
// $raw_tag = false;
// $html = '';
// foreach ($matches as $token)
// {
// $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
// $content = $token[0];
// if (is_null($tag))
// {
// if ( !empty($token['script']) )
// {
// $strip = $this->flhm_compress_js;
// }
// else if ( !empty($token['style']) )
// {
// $strip = $this->flhm_compress_css;
// }
// else if ($content == '<!--wp-html-compression no compression-->')
// {
// $overriding = !$overriding; 
// continue;
// }
// else if ($this->flhm_remove_comments)
// {
// if (!$overriding && $raw_tag != 'textarea')
// {
// $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
// }
// }
// }
// else
// {
// if ($tag == 'pre' || $tag == 'textarea')
// {
// $raw_tag = $tag;
// }
// else if ($tag == '/pre' || $tag == '/textarea')
// {
// $raw_tag = false;
// }
// else
// {
// if ($raw_tag || $overriding)
// {
// $strip = false;
// }
// else
// {
// $strip = true; 
// $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
// $content = str_replace(' />', '/>', $content);
// }
// }
// } 
// if ($strip)
// {
// $content = $this->flhm_removeWhiteSpace($content);
// }
// $html .= $content;
// } 
// return $html;
// } 
// public function flhm_parseHTML($html)
// {
// $this->html = $this->flhm_minifyHTML($html);
// if ($this->flhm_info_comment)
// {
// $this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
// }
// }
// protected function flhm_removeWhiteSpace($str)
// {
// $str = str_replace("\t", ' ', $str);
// $str = str_replace("\n",  '', $str);
// $str = str_replace("\r",  '', $str);
// while (stristr($str, '  '))
// {
// $str = str_replace('  ', ' ', $str);
// }   
// return $str;
// }
// }
// function flhm_wp_html_compression_finish($html)
// {
// return new FLHM_HTML_Compression($html);
// }
// function flhm_wp_html_compression_start()
// {
// ob_start('flhm_wp_html_compression_finish');
// }
// add_action('get_header', 'flhm_wp_html_compression_start');


?>
