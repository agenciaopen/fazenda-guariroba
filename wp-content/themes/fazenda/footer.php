<!-- Footer -->
<footer class="footer">

    <section class="footer-newsletter" id="inscreva">
        <div class="container">
            <h2 class="footer-newsletter-titulo"><?php the_field('titulo_newsletter', 'option'); ?></h2>
            <p class="footer-newsletter-texto">
                <?php the_field('descricao_newsletter', 'option'); ?>
            </p>

            <?php the_field('formulario_newsletter', 'option'); ?>


        </div>
    </section>


    <section class="footer-contato">
        <div class="container">
            <article class="footer-contato-article">

                <span class="footer-contato-spanum col-12">
                    <img src="<?php the_field('logo_site', 'option'); ?>" alt="" srcset="" class="footer-contato-img">
                    <?php
                    wp_nav_menu(array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'menu_class'        => 'footer-contato-lista',
                    ));
                    ?>

                </span>
                <span class="footer-contato-spandois col-12">
                    <ul class="footer-contato-telefone">

                        <?php
                        $telefone1 = get_field('telefone_1', 'option');
                        $phone = preg_replace('/\D+/', '', $telefone1);
                        $telefone2 = get_field('telefone_2', 'option');
                        $phone2 = preg_replace('/\D+/', '', $telefone2);
                        $email = get_field('e-mail_de_contato', 'option');
                        ?>

                        <?php if (get_field('telefone_1', 'option')) : ?>

                            <li>
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path d="M13.669 2.32468C12.1653 0.825774 10.1638 0 8.03395 0C3.64356 0 0.0678925 3.55583 0.0678925 7.92743C0.0678925 9.32374 0.435015 10.6875 1.13154 11.8911L0 16L4.22442 14.8965C5.38865 15.5271 6.69873 15.8599 8.03143 15.8624H8.03395C12.4243 15.8624 16 12.3065 16 7.93494C16.0025 5.81545 15.1727 3.82358 13.669 2.32468ZM8.03395 14.5236H8.03143C6.84206 14.5236 5.67782 14.2058 4.66195 13.6053L4.42056 13.4626L1.91356 14.1157L2.58243 11.6835L2.42401 11.4332C1.76018 10.3847 1.41066 9.1736 1.41317 7.92743C1.41317 4.29403 4.38284 1.33876 8.03646 1.33876C9.80418 1.33876 11.4688 2.0244 12.7185 3.27057C13.9683 4.51673 14.6572 6.17078 14.6572 7.93244C14.6547 11.5658 11.6825 14.5236 8.03395 14.5236ZM11.6649 9.58649C11.4663 9.48639 10.4881 9.00844 10.3046 8.94088C10.121 8.87582 9.99026 8.84079 9.85699 9.04098C9.72372 9.23866 9.34402 9.68658 9.22584 9.8167C9.11017 9.94933 8.9945 9.96434 8.79334 9.86675C8.59469 9.76666 7.95348 9.55896 7.19158 8.88333C6.60066 8.35783 6.20085 7.70973 6.08518 7.51204C5.96951 7.31436 6.08518 7.21677 6.17319 7.10666C6.38944 6.84141 6.60569 6.56115 6.67107 6.42853C6.73644 6.2959 6.70376 6.18079 6.65347 6.0807C6.60318 5.98061 6.20588 5.00719 6.03992 4.60932C5.87899 4.22396 5.71303 4.27651 5.59233 4.269C5.47666 4.264 5.34339 4.2615 5.21012 4.2615C5.07685 4.2615 4.8606 4.31154 4.67955 4.50923C4.49599 4.70691 3.98303 5.18736 3.98303 6.16078C3.98303 7.13419 4.69716 8.07757 4.79522 8.2102C4.8958 8.34282 6.19833 10.3422 8.19488 11.2005C8.67012 11.4057 9.03976 11.5258 9.32893 11.6184C9.80669 11.7685 10.2392 11.7485 10.5837 11.696C10.9659 11.6384 11.7605 11.2155 11.9264 10.7551C12.0924 10.2921 12.0924 9.89678 12.0421 9.8142C11.9969 9.73663 11.8636 9.68658 11.6649 9.58649Z" fill="white" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="16" height="16" fill="white" />
                                        </clipPath>
                                    </defs>
                                </svg>
                                <a href="tel:+55<?php echo $phone ?>" rel="noopener noreferrer"><?php echo $telefone1 ?></a>
                            </li>
                        <?php endif ?>
                        <?php if (get_field('telefone_2', 'option')) : ?>
                            <li>
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.5788 11.8534C15.4685 11.8534 14.3805 11.6802 13.3501 11.3383C12.8483 11.1651 12.2754 11.2983 11.9467 11.6358L9.90378 13.1813C7.55885 11.9289 6.05773 10.4278 4.82309 8.10067L6.3242 6.10659C6.7017 5.72909 6.83938 5.17395 6.67506 4.65877C6.33309 3.61954 6.15988 2.53146 6.15988 1.42117C6.15988 0.639526 5.52036 0 4.73871 0H1.42117C0.639526 0 0 0.639526 0 1.42117C0 10.5611 7.43893 18 16.5788 18C17.3605 18 18 17.3605 18 16.5788V13.2702C18 12.4885 17.3649 11.8534 16.5788 11.8534Z" fill="white" />
                                </svg>
                                <a href="tel:+55<?php echo $phone2 ?>" rel="noopener noreferrer"><?php echo $telefone2 ?></a>
                            </li>
                        <?php endif ?>

                    </ul>
                    <a class="footer-contato-email" href="mailto:<?php echo $email ?>" rel="noopener noreferrer"><?php echo $email ?></a>

                    <figure class="footer-contato-redes-sociais">
                        <?php if (get_field('facebook', 'option')) : ?>
                            <a href="<?php the_field('facebook', 'option'); ?>" target="_blank" rel="noopener noreferrer" class="link-redes"><img src="<?php echo get_template_directory_uri(); ?>/imgs/facebook.png" alt="" srcset="" class="footer-redes-sociais"></a>
                        <?php endif; ?>
                        <?php if (get_field('instagram', 'option')) : ?>
                            <a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="link-redes"><img src="<?php echo get_template_directory_uri(); ?>/imgs/instagram.png" alt="" srcset="" class="footer-redes-sociais"></a>
                        <?php endif; ?>

                    </figure>
                    
                </span>
               
            </article>
            <h6 class="texto-open"><?php the_field('open_texto', 'option'); ?></h6>
        </div>
    </section>
</footer>


<?php wp_footer(); ?>

</body>

</html>