<?php

/**
 *
 * Template Name: Home
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php if (wp_is_mobile()) : ?>
<?php if (get_field('imagem_destacada_mobile', $post->ID)) :
        $bg = get_field('imagem_destacada_mobile', $post->ID);
    else :
        $bg = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
    endif;
else :
    $bg = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
endif; ?>

<section class="home-inicial" style="background-image: url('<?php echo $bg; ?>')">
    <div class="container d-lg-flex flex-lg-column">
        <article class="home-inicial-article col-lg-7">
            <h1 class="home-inicial-titulo col-11 p-0 mb-4"><?php the_field('titulo', $page_ID); ?></h1>
            <?php $botao = get_field('botao', $page_ID); ?>
            <?php if ($botao) : ?>
                <a class="conhecer-produtos-link home-inicial-botao" href="<?php echo esc_url($botao['url']); ?>" target="<?php echo esc_attr($botao['target']); ?>">
                    <?php echo esc_html($botao['title']); ?>
                </a>
            <?php endif; ?>

        </article>
        <div class="p-0 align-self-end col-lg-10">
            <?php get_template_part('templates/global/template-part', 'selos'); ?>

        </div>
    </div>
</section>

<?php get_template_part('templates/global/template-part', 'produto-slide'); ?>

<?php get_template_part('templates/global/template-part', 'especial'); ?>


<section class="tipos-cafe">

    <div class="tipos-cafe-carrosel">
        <?php
        $args = array("posts_per_page" => -1, "post_type" => "cafes", 'order'   => 'ASC',);
        $posts_array = get_posts($args);
        foreach ($posts_array as $post) { ?>
            <article class="tipos-cafe-article mt-0">
                <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>
                <img src="<?php echo $url ?>" alt="" srcset="" class="tipos-cafe-img">
                <div class="tipos-cafe-paragrafo col-12 col-md-6">
                    <p class="tipos-cafe-titulo"><?php echo $post->post_title; ?></p>
                    <p class="tipos-cafe-texto col-8 p-0">
                        <?php echo $post->post_content; ?>
                    </p>
                </div>
                <div class="paginator-center text-color text-center d-md-none">
                    <ul class="d-flex justify-content-around d-md-none">
                        <li class="prev">
                            &#129172;</li>
                        <li class="next">
                            &#129174;</li>
                    </ul>
                </div>
            </article>

        <?php } ?>

        <?php wp_reset_postdata(); ?>

    </div>

</section>

<?php get_template_part('templates/global/template-part', 'qualidade-selo'); ?>
<?php get_template_part('templates/global/template-part', 'busca'); ?>
<?php get_template_part('templates/global/template-part', 'experiencia'); ?>


<section class="main instagram">
    <div class="container">
        <h2 class="instagram__titulo">Siga-nos no Instagram</h2>

        <?php echo do_shortcode('[instagram-feed]'); ?>
    </div>
</section>




<?php get_footer(); ?>