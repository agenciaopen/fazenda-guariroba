<?php
/**
*
* Template Name: Contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

    


<?php if ( wp_is_mobile() ) : ?>
    <?php if ( get_field( 'imagem_destacada_mobile', $post->ID ) ) :
        $bg = get_field( 'imagem_destacada_mobile', $post->ID ); 
        else: 
            $bg = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
        endif;
    else : 
    $bg = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
    endif; ?>
    
<section class="contato" style="background-image: url(<?php echo $bg;?>);">
    <div class="container">
        <article class="contato-article">
            <h1 class="contato-titulo mb-4"><?php the_field( 'titulo', $page_ID); ?></h1>
            <p class="contato-texto"> <?php the_field( 'sub-titulo', $page_ID); ?> </p>
            <p class="contato-texto-formulario"><?php the_field( 'texto', $page_ID ); ?></p>
        </article>
        <div class="contato-form">
            <?php echo do_shortcode('[contact-form-7 id="244" title="Contato"]');?>
        </div>

    </div>
</section>

<?php get_template_part( 'templates/global/template-part', 'busca' ); ?>
   

<?php get_footer(); ?>