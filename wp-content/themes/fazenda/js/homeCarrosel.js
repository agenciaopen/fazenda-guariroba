$('.tipos-cafe-carrosel').slick({
    dotsClass: "vertical-dots",
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 8000,
    slidesToShow: 1,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    responsive: [{
        breakpoint: 767,
        settings: {
            dots: false,
        }
    }, ]
});