var header = $("#nav_main");
$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        header.addClass("scrolled");
    } else {
        header.removeClass("scrolled");
    }
});



$('.produto-carousel').slick({
    dots: false,
    infinite: false,
    arrows: true,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
            breakpoint: 350,
            settings: {
                slidesToShow: 1.25,
                arrows: false,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1.5,
                arrows: false,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2.75,
            }

        }
    ]
});

$('.carousel-selo').slick({
    dots: false,
    arrows: false,
    slidesToShow: 5,
    responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                dots: true,
            }
        },
        {
            breakpoint: 499,
            settings: {
                slidesToShow: 1,
                dots: true,
            }
        },

    ]
});


$('.mundo-carrousel').slick({
    dots: false,
    infinite: false,
    arrows: false,
    slidesToShow: 5,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
        breakpoint: 767,
        settings: {
            slidesToShow: 1.3,
        }
    }, ]
});