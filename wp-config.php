<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'open_fazenda' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'x&PS/p=++JpKKE+i&6aaFiVp&QfLw5`<|{UUrEk^g!QlKML|^54:,}hw>AfKxMvk' );
define( 'SECURE_AUTH_KEY',   '}~XX/g]aGAN=Z4cG3) ;`MV2Hp56KD5k*m/. *0Z?Ib5,5;#tTrKR;@ }([h]y_R' );
define( 'LOGGED_IN_KEY',     '(0`;;IuTL3?~sU!k/D=O:n89*^b_ n=ek1>#yQ~9cl2qo1ZK5Fo`ALsu;3A{5/:[' );
define( 'NONCE_KEY',         ')~u!p:93`wFD;jNql PfU6>k&OnMgc>1tjM0^u5`I# @|F4|yH=0aVkG~xQ7@=_)' );
define( 'AUTH_SALT',         'P2%LJU*dWs/,QYDHrc>QfZ|ov>[;Ei^!j}3{X<xk;n| t5:tM({k=3jgiE76Jq-K' );
define( 'SECURE_AUTH_SALT',  'Q}uP>={H?y)FRYzFE, FZKtA,ouG=LlYGwVBLJB64<q;#]I_8.u;1bP8K50P`xE/' );
define( 'LOGGED_IN_SALT',    'l=W9z2cl|/K#^q*& _L}2*Q|ZA)lw0x1NP8}a@jIUEaDD~:Ka4j.}6dE|ab~vU)+' );
define( 'NONCE_SALT',        'M}l:Nr3LRagC[BDvAX,_4WU?jUvo,_r#6N ]yx5=RgrN7:7&?w5E(h_&T{!ei#ev' );
define( 'WP_CACHE_KEY_SALT', 'R%!X;7ghj,Ck8|&<*=ra{}o~OnbK07p3[TWj-b/}L}5s93v4*lUTHDK[&@gN:b@T' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
